class Tweet < ActiveRecord::Base
  validates :message, presence:true
  validates :message, length:{in: 1..139}
end
